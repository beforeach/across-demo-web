<!--
  Copyright 2014 Foreach bvba

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  -->
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
	<title>CWB demo website</title>
</head>
<body th:fragment="content">
    <h1>Registration form</h1>
    <div>
        <p>
            The form below is used by a specific feature in the CWB sample project. Web forms are a functionality that dare to change from time to time because fields are added, removed, extra checks are added, etc...
            Besides the fact that they change from time to time, they serve as an important communication channel with your users so they are good candidates for a CWB test.<br />
            CWB can easily test if the form still submits after changes, if the data is inserted in a database, and so on.
        </p>
        <p>
           To demonstrate how this could be achieved multiple tests are written against our test form to show how we could achieve this.
        </p>
    <div class="panel panel-default">
      <div class="panel-body">
<pre>
Scenario: Fill in the registration form
    # this scenario follows the happy flow, everything is filled in correctly
    Given i am on "http://across.foreach.com/demo/cwb/registrationform"
    Then i fill in text "firstname" with "John"
    And i fill in text "lastname" with "Doe"
    And i choose "15" in select "date"
    And i choose "june" in select "month"
    And i choose "1985" in select "year"
    And i fill in text "street" with "ruestreet 12"
    And i fill in text "postalcode" with "1070"
    And i check checkbox "mailyesno"
    And i click on radio "sex[1]"
    And i click on button "submit"
    # After the submitting of the form we wait on the answer to verify the submitted values
    And i wait until div "John" is visible
    And i should see div "/van der straeten/i"
    And i should see div "15/06/1969"
    And i should see div "ruestreet 12"
    And i should see div "1070"
    And checkbox "mailyesno" is checked
    And radio "sex[1]" is checked
</pre>
</div>
</div>
    <div class="panel panel-default">
      <div class="panel-body">
<pre>
Scenario: Do not fill in the registration form and submit
    # in this scenario nothing is filled in and I receive an error to fill in all the fields
   Given i am on "http://across.foreach.com/demo/cwb/registrationform"
   Then i click on button "submit"
   And i should see div "Please fill in all the fields"
</pre>
</div>
</div>
    <div class="panel panel-default">
      <div class="panel-body">
<pre>
Scenario: Fill in the registration form with wrong data
   # this scenario fills out every fields with wrong data and then corrects it
   Given i am on "http://across.foreach.com/demo/cwb/registrationform"
   Then i fill in text "firstname" with "1"
   And i should see div "Numbers are not allowed as a first name"
   Then i fill in text "firstname" with "John"

   # try to use * as lastname
   Then i fill in text "lastname" with "*"
   And i should see div "Only the characters a through z are allowed as a last name"
   Then i fill in text "lastname" with "Doe"

   And i choose "15" in select "date"
   And i choose "june" in select "month"
   And i choose "1985" in select "year"

   # try to use [ as a streetname
   And i fill in text "street" with "ruestreet [ "
   And i should see div "Only the characters a through z and numbers are allowed as a street name and house number"
   And i fill in text "street" with "ruestreet 12"

   # try to use L as input for a postalcode
   And i fill in text "postalcode" with "1070L"
   And i should see div "Only numbers are allowed as a postal code"
   And i fill in text "postalcode" with "1070"

   And i check checkbox "mailyesno"

   # Try to submit the form without a sex choice
   And i click on button "submit"
   Then i should see div "Please check if you are female, male or other"

   And i click on radio "sex[1]"
   And i click on button "submit"
</pre>
</div>
</div>
    </div>
    <div id="section">
        <form th:action="@{'/cwb/registrationform'}" th:object="${form}" method="POST">
            <div class="form-group">
                <label for="firstname">First name</label>
                <input th:field="*{firstname}" />
            </div>
            <div class="form-group">
                <label for="lastname">Last name</label>
                <input th:field="*{lastname}" />
            </div>
            <div class="form-group">
                <label for="date">Day</label>
                <select th:field="*{date}">
                    <option value=""></option>
                    <option th:value="1">1</option>
                    <option th:value="2">2</option>
                    <option th:value="3">3</option>
                    <option th:value="4">4</option>
                    <option th:value="5">5</option>
                    <option th:value="6">6</option>
                    <option th:value="7">7</option>
                    <option th:value="8">8</option>
                    <option th:value="9">9</option>
                    <option th:value="10">10</option>
                    <option th:value="11">11</option>
                    <option th:value="12">12</option>
                    <option th:value="13">13</option>
                    <option th:value="14">14</option>
                    <option th:value="15">15</option>
                    <option th:value="16">16</option>
                    <option th:value="17">17</option>
                    <option th:value="18">18</option>
                    <option th:value="19">19</option>
                    <option th:value="20">20</option>
                    <option th:value="21">21</option>
                    <option th:value="22">22</option>
                    <option th:value="23">23</option>
                    <option th:value="24">24</option>
                    <option th:value="25">25</option>
                    <option th:value="26">26</option>
                    <option th:value="27">27</option>
                    <option th:value="28">28</option>
                    <option th:value="29">29</option>
                    <option th:value="30">30</option>
                    <option th:value="31">31</option>
                </select>
                <label for="month">Month</label>
                <select th:field="*{month}">
                    <option value=""></option>
                    <option th:value="january">January</option>
                    <option th:value="february">February</option>
                    <option th:value="march">March</option>
                    <option th:value="april">April</option>
                    <option th:value="may">May</option>
                    <option th:value="june">June</option>
                    <option th:value="july">July</option>
                    <option th:value="august">August</option>
                    <option th:value="september">September</option>
                    <option th:value="october">October</option>
                    <option th:value="november">November</option>
                    <option th:value="december">December</option>
                </select>
                <label for="year">Year</label>
                <select th:field="*{year}">
                    <option value=""></option>
                    <option th:value="1980">1980</option>
                    <option th:value="1981">1981</option>
                    <option th:value="1982">1982</option>
                    <option th:value="1983">1983</option>
                    <option th:value="1984">1984</option>
                    <option th:value="1985">1985</option>
                    <option th:value="1986">1986</option>
                    <option th:value="1987">1987</option>
                    <option th:value="1988">1988</option>
                    <option th:value="1989">1989</option>
                    <option th:value="1990">1990</option>
                    <option th:value="1991">1991</option>
                    <option th:value="1992">1992</option>
                    <option th:value="1993">1993</option>
                    <option th:value="1994">1994</option>
                    <option th:value="1995">1995</option>
                    <option th:value="1996">1996</option>
                    <option th:value="1997">1997</option>
                    <option th:value="1998">1998</option>
                    <option th:value="1999">1999</option>
                    <option th:value="2000">2000</option>
                    <option th:value="2001">2001</option>
                    <option th:value="2002">2002</option>
                    <option th:value="2003">2003</option>
                    <option th:value="2004">2004</option>
                    <option th:value="2005">2005</option>
                    <option th:value="2006">2006</option>
                    <option th:value="2007">2007</option>
                    <option th:value="2008">2008</option>
                    <option th:value="2009">2009</option>
                    <option th:value="2010">2010</option>
                    <option th:value="2011">2011</option>
                    <option th:value="2012">2012</option>
                    <option th:value="2013">2013</option>
                </select>
            </div>
            <div class="form-group">
                <label for="street">Street</label>
                <input th:field="*{street}" />
            </div>
            <div class="form-group">
                <label for="postalcode">Postal code</label>
                <input th:field="*{postalcode}" />
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" th:field="*{mailyesno}" /> Mail?
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="radio">
                    <label>
                        <input type="radio" value="male" th:field="*{sex}" /> Male
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" value="female" th:field="*{sex}" /> Female
                    </label>
                </div>
            </div>
            <button type="submit" name="submit" class="btn btn-default">Submit</button>
        </form>

        <div th:switch="${formCompleted}">
          <p th:case="true">
            <div th:unless="${#lists.isEmpty(formErrors)}">
                <p th:each="formError : ${formErrors}">
                    <span th:text="'Error in ' + ${formError}"></span>
                </p>
            </div>
            <div th:if="${#lists.isEmpty(formErrors)}">
                <span th:text="${form.firstname}"></span><br />
                <span th:text="${form.lastname}"></span><br />
                <span th:text="${form.date}"></span><br />
                <span th:text="${form.month}"></span><br />
                <span th:text="${form.year}"></span><br />
                <span th:text="${form.street}"></span><br />
                <span th:text="${form.postalcode}"></span><br />
                <span th:text="${form.mailyesno}"></span><br />
                <span th:text="${form.sex}"></span>
            </div>
          </p>
          <p th:case="false">Please fill in all the fields</p>
        </div>
    </div>
</body>
</html>
