/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.adminweb.AdminWebModuleSettings;
import org.springframework.context.annotation.Configuration;

/**
 * Add AdminWebModule.  This activates support for AdminWebController controllers that provide
 * administration web interfaces to the user.  Much like the DebugWebModule, these will only be
 * available if the AdminWebModule is active and they can build on top of the default admin web interface
 * using the Spring security support.
 */
@Configuration
public class AdminWebConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		AdminWebModule adminWebModule = new AdminWebModule();

		// Configure the base path for the administrative interface.  When accessing child of this path,
		// the user must be logged in and have the valid permissions.  Apart from controller specific security
		// the user must have at least the "access administration" permission.
		adminWebModule.setRootPath( "/secure" );

		adminWebModule.setProperty( AdminWebModuleSettings.REMEMBER_ME_KEY, "test" );

		context.addModule( adminWebModule );
	}
}
