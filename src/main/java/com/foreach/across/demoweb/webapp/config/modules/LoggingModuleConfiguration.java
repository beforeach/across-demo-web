/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.logging.LoggingModule;
import com.foreach.across.modules.logging.LoggingModuleSettings;
import com.foreach.across.modules.logging.requestresponse.RequestResponseLogConfiguration;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @author Andy Somers
 */
@Configuration
public class LoggingModuleConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext acrossContext ) {
		LoggingModule loggingModule = new LoggingModule();
		loggingModule.setProperty( LoggingModuleSettings.REQUEST_RESPONSE_LOG_ENABLED, true );

		RequestResponseLogConfiguration logConfiguration = new RequestResponseLogConfiguration();
		logConfiguration.setExcludedPathPatterns( Arrays.asList( "/static/**", "/debug/**" ) );

		loggingModule.setProperty( LoggingModuleSettings.REQUEST_RESPONSE_LOG_CONFIGURATION, logConfiguration );

		acrossContext.addModule( loggingModule );
	}
}
