/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.spring.security.SpringSecurityModule;
import com.foreach.across.modules.spring.security.acl.SpringSecurityAclModule;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Add SpringSecurityModule.  This module configures Spring security for the entire AcrossContext.
 * This will enable method based security through annotations in all modules.  In a web context where
 * Thymeleaf is enabled and the Thymeleaf Spring security package is available, the support for it
 * will automatically be activated as well.</p>
 *
 * @author Arne Vandamme
 * @see com.foreach.across.modules.spring.security.SpringSecurityModule
 */
@Configuration
public class SpringSecurityConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		SpringSecurityModule springSecurityModule = new SpringSecurityModule();

		context.addModule( springSecurityModule );
		context.addModule( new SpringSecurityAclModule() );
	}
}
