package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.module.applicationinfo.ApplicationInfoModule;
import com.foreach.across.module.applicationinfo.ApplicationInfoModuleSettings;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

@Configuration
public class ApplicationInfoConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		ApplicationInfoModule applicationInfoModule = new ApplicationInfoModule();
		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.APPLICATION_ID, "demo-webapp");
		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.APPLICATION_NAME, "Across Demo Webapplication");
		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.ENVIRONMENT_ID, "demo");
		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.ENVIRONMENT_NAME, "Demo environment");

		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.BUILD_ID, "demo-webapp");
		applicationInfoModule.setProperty( ApplicationInfoModuleSettings.BUILD_DATE, new Date());

		context.addModule( applicationInfoModule );
	}
}
