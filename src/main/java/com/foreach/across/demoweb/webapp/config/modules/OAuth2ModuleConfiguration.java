/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.oauth2.OAuth2Module;
import org.springframework.context.annotation.Configuration;

/**
 * Configures the OAuth2Module that is used to enable OAuth support on the API REST services.
 * The demo web site also installs a default OAuth2 client.
 *
 * @author Arne Vandamme
 * @see com.foreach.across.demoweb.module.installers.DemoOAuth2ClientInstaller
 * @since 1.0.3
 */
@Configuration
public class OAuth2ModuleConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext acrossContext ) {
		OAuth2Module oAuth2Module = new OAuth2Module();

		acrossContext.addModule( oAuth2Module );
	}
}
