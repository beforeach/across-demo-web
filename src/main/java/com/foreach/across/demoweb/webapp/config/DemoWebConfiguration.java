/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.config.EnableAcrossContext;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.demoweb.module.DemoWebModule;
import com.foreach.common.spring.logging.LogbackConfigurer;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;

/**
 * <p>Configures the Demo web application.</p>
 * <p>All modules are configured through a separate configurer in the subpackage "modules".
 * You can easily configure several modules inside a single configurer, but for the demo
 * web application they have been split for improved documenting and readability.</p>
 *
 * @author Arne Vandamme
 */
@Configuration
@ComponentScan("com.foreach.across.demoweb.webapp.config.modules")
@PropertySources({
		                 @PropertySource("classpath:/config/demoweb.properties")
                 })
@EnableAcrossContext
public class DemoWebConfiguration implements AcrossContextConfigurer
{
	@Autowired
	private Environment environment;

	@Bean
	public DataSource acrossDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName( "org.hsqldb.jdbc.JDBCDriver" );
		dataSource.setUrl( "jdbc:hsqldb:mem:/hsql/acrossDemoWeb" );
		dataSource.setUsername( "sa" );
		dataSource.setPassword( "" );

		return dataSource;
	}

	@Override
	public void configure( AcrossContext context ) {
		// Enable development mode, this will pickup the across-devel.properties in ${user.home}/dev-configs
		// and will configure physical paths for module resources if they match
		context.setDevelopmentMode( true );

		// The DemoWebModule contains all specific application classes and controllers.
		// Encapsulating everything *except* the AcrossContext configuration inside modules
		// is considered a best practice.
		context.addModule( new DemoWebModule() );
	}

	@Bean
	public LogbackConfigurer logbackConfigurer(
			@Value("classpath:/config/logback.xml") Resource defaultConfig,
			@Value("classpath:/config/${environment.type}/logback.xml") Resource environmentConfig
	) {
		return new LogbackConfigurer(
				environment.getRequiredProperty( "log.dir" ),
				defaultConfig,
				environmentConfig
		);
	}
}
