/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.hibernate.AcrossHibernateModule;
import com.foreach.across.modules.hibernate.AcrossHibernateModuleSettings;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Add AcrossHibernateModule.  This module enables support for Hibernate in other modules
 * and is required by the UserModule.  Modules that want to build on top of Hibernate can provide
 * package definitions to the AcrossHibernate module.  These packages will then be included
 * when the AcrossHibernateModule boots and builds the sessionFactory.</p>
 * <p>In a multi-database configuration, you could can extend AcrossHibernateModule to
 * define other Hibernate configuration sets with a different data source.  Since no data source
 * is specified in this configuration, the AcrossContext data source is used.</p>
 *
 * @author Arne Vandamme
 * @see com.foreach.across.modules.hibernate.AcrossHibernateModule
 * @see com.foreach.across.modules.hibernate.provider.HibernatePackageProvider
 * @see com.foreach.across.core.database.SchemaConfiguration
 */
@Configuration
public class AcrossHibernateConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		AcrossHibernateModule hibernateModule = new AcrossHibernateModule();
		hibernateModule.setHibernateProperty( AvailableSettings.AUTOCOMMIT, "false" );

		hibernateModule.setProperty( AcrossHibernateModuleSettings.OPEN_SESSION_IN_VIEW_INTERCEPTOR, true );

		context.addModule( hibernateModule );
	}
}
