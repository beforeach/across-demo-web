/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.debugweb.DebugWebModule;
import org.springframework.context.annotation.Configuration;

/**
 * <p>Add DebugWebModule.  DebugWeb provides a set of web controllers useful for debugging the running application.
 * It also provides an infrastructure for other modules to create their own controllers (DebugWebController annotated
 * controllers) that will only be available if the DebugWebModule is present and active.</p>
 * <p>Default debug controllers include an Application/Across context browser and a form to modify the levels
 * of the configured loggers.</p>
 *
 * @author Arne Vandamme
 * @see com.foreach.across.modules.debugweb.DebugWebModule
 * @see com.foreach.across.modules.debugweb.mvc.DebugWebController
 * @see com.foreach.across.modules.debugweb.DebugWeb
 */
@Configuration
public class DebugWebConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		DebugWebModule debugWebModule = new DebugWebModule();

		// Specify the web path where the debug controllers are available.
		// This can be configured and all correctly implemented debug web controllers will automatically
		// be relocated under the path specified
		debugWebModule.setRootPath( "/debug" );

		context.addModule( debugWebModule );
	}
}
