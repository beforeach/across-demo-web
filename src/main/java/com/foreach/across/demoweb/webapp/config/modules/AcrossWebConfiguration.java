/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.webapp.config.modules;

import com.foreach.across.config.AcrossContextConfigurer;
import com.foreach.across.core.AcrossContext;
import com.foreach.across.modules.web.AcrossWebModule;
import com.foreach.across.modules.web.AcrossWebModuleSettings;
import com.foreach.across.modules.web.AcrossWebViewSupport;
import com.foreach.across.modules.web.config.multipart.MultipartConfiguration;
import org.springframework.context.annotation.Configuration;

/**
 * @author Arne Vandamme
 */
@Configuration
public class AcrossWebConfiguration implements AcrossContextConfigurer
{
	@Override
	public void configure( AcrossContext context ) {
		AcrossWebModule webModule = new AcrossWebModule();
		webModule.setViewsResourcePath( "/static" );
		webModule.setSupportViews( AcrossWebViewSupport.THYMELEAF );

		webModule.setProperty( AcrossWebModuleSettings.MULTIPART_AUTO_CONFIGURE, true );
		webModule.setProperty( AcrossWebModuleSettings.MULTIPART_SETTINGS, new MultipartConfiguration( System.getProperty( "java.io.tmpdir" ), 100000, -1L, 10 * 1024 ) );

		context.addModule( webModule );
	}
}
