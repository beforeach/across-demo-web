/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.config.security;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.spring.security.configuration.SpringSecurityWebConfigurerAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * Configures the test REST services used for Cucumber Web Bridge to be non-secured,
 * and not to use CSRF.
 *
 * @author Arne Vandamme
 */
@Configuration
@OrderInModule(2)
public class RestServicesConfiguration extends SpringSecurityWebConfigurerAdapter
{
	@Override
	public void configure( HttpSecurity http ) throws Exception {
		http.antMatcher( "/rest/**" )
		    .authorizeRequests().anyRequest().permitAll()
		    .and()
		    .csrf().disable();
	}
}
