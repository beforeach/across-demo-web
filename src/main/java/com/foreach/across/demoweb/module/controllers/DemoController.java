/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController
{
	@RequestMapping("/hello")
	@ResponseBody
	public String hello() {
		return "hello";
	}

	@RequestMapping("/ok")
	@ResponseBody
	public String ok() {
		return "always ok";
	}

	@RequestMapping("/secure/secured")
	@ResponseBody
	@PreAuthorize("hasRole('ROLE_admin')")
	public String secured() {
		return "you can see the secured page";
	}

	@RequestMapping("/secure/role")
	@ResponseBody
	@PreAuthorize("hasRole('admin')")
	public String role() {
		return "secured on administrator role";
	}

	@RequestMapping("/secure/perm")
	@ResponseBody
	@PreAuthorize("hasAuthority('manage users')")
	public String perm() {
		return "secured on permission";
	}

	@RequestMapping("/secure/fail")
	@ResponseBody
	@PreAuthorize("1 == 0")
	public String shouldNeverSee() {
		return "you should not see this...";
	}

	@RequestMapping("/redirect")
	public String redirect() {
		return "redirect:http://www.google.be";
	}
}
