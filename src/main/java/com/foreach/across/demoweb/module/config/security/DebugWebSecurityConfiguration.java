/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.config.security;

import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.debugweb.DebugWeb;
import com.foreach.across.modules.spring.security.configuration.SpringSecurityWebConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@AcrossDepends(required = "DebugWebModule")
@OrderInModule(1)
public class DebugWebSecurityConfiguration extends SpringSecurityWebConfigurerAdapter
{
	@Autowired
	private DebugWeb debugWeb;

	@Override
	public void configure( AuthenticationManagerBuilder auth ) throws Exception {
		auth.inMemoryAuthentication().withUser( "debug" ).password( "debug" ).roles( "DEBUG_USER" );
	}

	@Override
	public void configure( HttpSecurity http ) throws Exception {
		http.antMatcher( debugWeb.path( "/**" ) )
		    .authorizeRequests().anyRequest().hasRole( "DEBUG_USER" )
		    .and()
		    .formLogin().disable()
		    .httpBasic()
		    .and()
		    .sessionManagement().sessionCreationPolicy( SessionCreationPolicy.STATELESS )
		    .and()
		    .csrf().disable();
	}
}
