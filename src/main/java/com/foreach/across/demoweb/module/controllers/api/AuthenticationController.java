/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.api;

import com.foreach.across.modules.user.business.User;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Exposes a sample REST method that gives basic information on the principal attached
 * to a valid OAuth2 authentication.
 *
 * @author Arne Vandamme
 * @see com.foreach.across.demoweb.module.config.security.OAuth2ResourcesConfiguration
 */
@RestController
@RequestMapping("/api")
public class AuthenticationController
{
	@RequestMapping(value = "/principal", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> principalInfo( OAuth2Authentication authentication ) {
		Map<String, String> principalInfo = new HashMap<>();

		if ( authentication.isClientOnly() ) {
			principalInfo.put( "authentication_type", "client" );
			principalInfo.put( "client_id", ObjectUtils.toString( authentication.getPrincipal() ) );
		}
		else {
			principalInfo.put( "authentication_type", "user" );
			principalInfo.put( "username", ( (User) authentication.getPrincipal() ).getUsername() );
		}

		return new ResponseEntity<>( principalInfo, HttpStatus.OK );
	}
}
