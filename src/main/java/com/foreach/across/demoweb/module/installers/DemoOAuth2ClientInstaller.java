/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.modules.oauth2.business.OAuth2Client;
import com.foreach.across.modules.oauth2.services.OAuth2Service;
import com.foreach.across.modules.user.business.Role;
import com.foreach.across.modules.user.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Arne Vandamme
 */
@Installer(description = "Create the demo OAuth2 client.",
           phase = InstallerPhase.AfterModuleBootstrap)
public class DemoOAuth2ClientInstaller
{

	@Autowired
	private OAuth2Service oAuth2Service;

	@Autowired
	private RoleService roleService;

	@InstallerMethod
	public void createClient() {
		// Ensure the OAuth2 client role exists
		Role clientRole =
				roleService.defineRole( "ROLE_OAUTH2_CLIENT", "OAuth2 Client", Arrays.asList( "manage users" ) );

		// Create client with id demo_client and secret demo
		OAuth2Client clientById = oAuth2Service.getClientById( "demo_client" );

		if ( clientById == null ) {
			Set<Role> roles = new HashSet<>();
			roles.add( clientRole );

			OAuth2Client client = new OAuth2Client();
			client.setClientId( "demo_client" );
			client.setClientSecret( "demo" );
			client.setSecretRequired( true );

			Set<String> grantTypes = new HashSet<>();
			grantTypes.add( "authorization_code" );
			grantTypes.add( "implicit" );
			grantTypes.add( "client_credentials" );
			grantTypes.add( "password" );
			grantTypes.add( "refresh_token" );
			client.getAuthorizedGrantTypes().addAll( grantTypes );

			Set<String> resourceIds = new HashSet<>();
			resourceIds.add( "demo-web" );
			client.getResourceIds().addAll( resourceIds );
			client.getRoles().addAll( roles );

			oAuth2Service.save( client );
		}
	}
}
