/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.cwb.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

public class ComplexObject
{
	private Detail detail;
	private List<Option> options;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<List> listOfLists;

	public Detail getDetail() {
		return detail;
	}

	public void setDetail( Detail detail ) {
		this.detail = detail;
	}

	public List<Option> getOptions() {
		return options;
	}

	public void setOptions( List<Option> options ) {
		this.options = options;
	}

	public List<List> getListOfLists() {
		return listOfLists;
	}

	public void setListOfLists( List<List> listOfLists ) {
		this.listOfLists = listOfLists;
	}
}
