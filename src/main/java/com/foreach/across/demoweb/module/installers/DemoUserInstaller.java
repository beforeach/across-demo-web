/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.modules.user.business.Role;
import com.foreach.across.modules.user.dto.UserDto;
import com.foreach.across.modules.user.services.RoleService;
import com.foreach.across.modules.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Set;

@Installer(description = "Create the demo user with the register user role.",
           phase = InstallerPhase.AfterModuleBootstrap)
public class DemoUserInstaller
{
	@Autowired
	private UserService userService;

	@Autowired
	private RoleService roleService;

	@InstallerMethod
	public void createUser() {
		UserDto user = new UserDto();
		user.setUsername( "demo" );
		user.setPassword( "demo" );
		user.setEmail( "demo@localhost" );
		user.setFirstName( "" );
		user.setLastName( "" );
		user.setDisplayName( "Demo user" );

		Set<Role> roles = new HashSet<>();
		roles.add( roleService.getRole( "ROLE_REGISTERED_USER" ) );

		user.setRoles( roles );

		userService.save( user );
	}
}
