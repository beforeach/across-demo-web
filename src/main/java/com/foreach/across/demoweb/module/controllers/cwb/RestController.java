/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.cwb;

import com.foreach.across.demoweb.module.controllers.cwb.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;
import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.LMinMax;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.*;

@org.springframework.web.bind.annotation.RestController
@RequestMapping({ "/rest" })
public class RestController
{
	@InitBinder
	public void initBinder( ServletRequestDataBinder binder ) {
		binder.registerCustomEditor( byte[].class, new ByteArrayMultipartFileEditor() );
	}

	@RequestMapping("/complexList")
	public Collection<ComplexObject> getComplexListResponse() {
		Collection<ComplexObject> list = new ArrayList<>();
		ComplexObject complexObject = new ComplexObject();
		Detail detail = new Detail();
		detail.setQuestion( "this is a question" );
		Answers answers = new Answers();
		answers.setAnswer( "this is answer 1" );
		detail.setAnswers( answers );
		complexObject.setDetail( detail );
		List<Option> options = new ArrayList<>();
		Option option1 = new Option();
		option1.setOption1( "this is option 1" );
		options.add( option1 );
		Option option2 = new Option();
		option2.setOption2( "this is option 2" );
		options.add( option2 );
		complexObject.setOptions( options );
		List<List> listOfLists = new ArrayList<>();
		List<Option> options1 = new ArrayList<>();
		Option option3 = new Option();
		option3.setOption1( "option 1" );
		options1.add( option3 );
		Option option4 = new Option();
		option4.setOption2( "option 2" );
		options1.add( option4 );
		listOfLists.add( options1 );
		List<Value> values = new ArrayList<>();
		Value value1 = new Value();
		value1.setValue1( "this is value 1" );
		values.add( value1 );
		Value value2 = new Value();
		value2.setValue2( "this is value 2" );
		listOfLists.add( values );
		complexObject.setListOfLists( listOfLists );
		list.add( complexObject );
		return list;
	}

	@RequestMapping("/listOfLists")
	public Collection<Collection> getListOfListsResponse() {
		Option option1 = new Option();
		option1.setOption1( "option 1" );
		Option option2 = new Option();
		option2.setOption2( "option 2" );
		Collection<Option> list1 = new ArrayList<>();
		list1.add( option1 );
		list1.add( option2 );

		Value value1 = new Value();
		value1.setValue1( "value 1" );
		Value value2 = new Value();
		value2.setValue2( "value 2" );
		Collection<Value> list2 = new ArrayList<>();
		list2.add( value1 );
		list2.add( value2 );

		Collection<Collection> fullList = new ArrayList<>();
		fullList.add( list1 );
		fullList.add( list2 );
		return fullList;
	}

	@RequestMapping("/complexObject")
	public ComplexObject getComplexObjectResponse() {
		ComplexObject complexObject = new ComplexObject();
		Detail detail = new Detail();
		detail.setQuestion( "this is a question" );
		Answers answers = new Answers();
		answers.setAnswer( "this is answer 1" );
		detail.setAnswers( answers );
		complexObject.setDetail( detail );
		List<Option> options = new ArrayList<>();
		Option option1 = new Option();
		option1.setOption1( "this is option 1" );
		Option option2 = new Option();
		option2.setOption2( "this is option 2" );
		options.add( option2 );
		options.add( option1 );
		complexObject.setOptions( options );
		return complexObject;
	}

	@RequestMapping("/simpleObject")
	public ResponseEntity<UserResponse> getSimpleObject() {
		User user = new User();
		user.setUsername( "regular-one" );
		user.setEmail( "regular@gmail.com" );
		UserResponse response = new UserResponse();
		response.setUser( user );
		return new ResponseEntity<>( response, HttpStatus.OK );
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity postUpload( @RequestParam("content") byte[] file ) {
		if ( file != null && file.length > 0 ) {
			return new ResponseEntity( HttpStatus.OK );
		}
		return new ResponseEntity( HttpStatus.UNPROCESSABLE_ENTITY );
	}

	@RequestMapping(value = "/upload", method = RequestMethod.PUT)
	public ResponseEntity putUpload( @RequestParam("content") byte[] file ) {
		if ( file != null && file.length > 0 ) {
			return new ResponseEntity( HttpStatus.OK );
		}
		return new ResponseEntity( HttpStatus.UNPROCESSABLE_ENTITY );
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public ResponseEntity<UserResponse> post( @RequestBody User user ) {
		UserResponse response = new UserResponse();
		User returnUser = new User();
		BeanUtils.copyProperties( user, returnUser );
		response.setUser( returnUser );
		return new ResponseEntity<>( response, HttpStatus.OK );
	}

	@RequestMapping(value = "/csv", method = RequestMethod.GET)
	public void csv( HttpServletResponse response,
	                 @RequestParam(value = "withHeader", required = false) Boolean withHeader,
	                 @RequestParam(value = "quoteChar", required = false) String quoteChar,
	                 @RequestParam(value = "delimiter", required = false) String delimiter,
	                 @RequestParam(value = "endOfLineSymbols",
	                               required = false) String endOfLineSymbols ) throws Exception {
		char csvQuoteChar = (char) CsvPreference.STANDARD_PREFERENCE.getQuoteChar();
		char csvDelimiter = (char) CsvPreference.STANDARD_PREFERENCE.getDelimiterChar();
		String csvEndOfLineString = CsvPreference.STANDARD_PREFERENCE.getEndOfLineSymbols();
		if ( StringUtils.isNotEmpty( delimiter ) ) {
			csvDelimiter = delimiter.toCharArray()[0];
		}
		if ( StringUtils.isNotEmpty( quoteChar ) ) {
			csvQuoteChar = quoteChar.toCharArray()[0];
		}
		if ( StringUtils.isNotEmpty( endOfLineSymbols ) ) {
			csvEndOfLineString = endOfLineSymbols;
		}
		ServletOutputStream outputStream = response.getOutputStream();
		response.setHeader( "Content-Type", "text/csv" );
		writeWithCsvListWriter( outputStream, withHeader, csvDelimiter, csvQuoteChar, csvEndOfLineString );
	}

	private static void writeWithCsvListWriter( OutputStream outputStream,
	                                            Boolean withHeader,
	                                            char csvDelimiter,
	                                            char quoteChar,
	                                            String endOfLineString ) throws Exception {

		// create the customer Lists (CsvListWriter also accepts arrays!)
		final List<Object> john = Arrays.asList( new Object[] { "1", "John", "Dunbar",
		                                                        new GregorianCalendar( 1945, Calendar.JUNE,
		                                                                               13 ).getTime(),
		                                                        "1600 Amphitheatre Parkway\nMountain View, CA 94043\nUnited States",
		                                                        null, null,
		                                                        "\"May the Force be with you.\" - Star Wars",
		                                                        "jdunbar@gmail.com", 0L } );

		final List<Object> bob = Arrays.asList( new Object[] { "2", "Bob", "Down",
		                                                       new GregorianCalendar( 1919, Calendar.FEBRUARY,
		                                                                              25 ).getTime(),
		                                                       "1601 Willow Rd.\nMenlo Park, CA 94025\nUnited States",
		                                                       true, 0,
		                                                       "\"Frankly, my dear, I don't give a damn.\" - Gone With The Wind",
		                                                       "bobdown@hotmail.com", 123456L } );

		ICsvListWriter listWriter = null;
		try {
			CsvPreference csvPreference = new CsvPreference.Builder( quoteChar, csvDelimiter, endOfLineString ).build();
			listWriter = new CsvListWriter( new OutputStreamWriter( outputStream ), csvPreference );

			final CellProcessor[] processors = getProcessors();
			final String[] header = new String[] { "customerNo", "firstName", "lastName", "birthDate",
			                                       "mailingAddress", "married", "numberOfKids", "favouriteQuote",
			                                       "email", "loyaltyPoints" };

			// write the header
			if ( Boolean.TRUE == withHeader ) {
				listWriter.writeHeader( header );
			}

			// write the customer lists
			listWriter.write( john, processors );
			listWriter.write( bob, processors );

		}
		finally {
			if ( listWriter != null ) {
				listWriter.close();
			}
		}
	}

	private static CellProcessor[] getProcessors() {

		return new CellProcessor[] { new UniqueHashCode(), // customerNo (must be unique)
		                             new NotNull(), // firstName
		                             new NotNull(), // lastName
		                             new FmtDate( "dd/MM/yyyy" ), // birthDate
		                             new NotNull(), // mailingAddress
		                             new Optional( new FmtBool( "Y", "N" ) ), // married
		                             new Optional(), // numberOfKids
		                             new NotNull(), // favouriteQuote
		                             new NotNull(), // email
		                             new LMinMax( 0L, LMinMax.MAX_LONG ) // loyaltyPoints
		};
	}
}
