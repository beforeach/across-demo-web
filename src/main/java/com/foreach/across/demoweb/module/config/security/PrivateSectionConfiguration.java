/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.config.security;

import com.foreach.across.core.annotations.OrderInModule;
import com.foreach.across.modules.spring.security.configuration.SpringSecurityWebConfigurerAdapter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Since this configurer contains a wildcard (anyRequest().permitAll()) rule, it should come
 * as the very last in the security filter chain.  More specific security rules can run before this one.
 * <p/>
 * Ensuring it is last is simply done by ordering it as the last in the module.  Since the module itself
 * depends on Oauth2Module and AdminWebModule, the security configurers defined there will have been run before.
 * <p/>
 * An alternative approach - without considering any other modules - would have been to use the @Order annotation.
 */
@Configuration
@OrderInModule(Ordered.LOWEST_PRECEDENCE)
public class PrivateSectionConfiguration extends SpringSecurityWebConfigurerAdapter
{
	@Override
	public void configure( HttpSecurity http ) throws Exception {
		http
				.authorizeRequests()
				.antMatchers( "/private/**" ).hasAuthority( "access private section" )
				.anyRequest().permitAll()
				.and()
				.formLogin().permitAll()
				.and()
				.logout()
				.invalidateHttpSession( true ).logoutRequestMatcher( new AntPathRequestMatcher( "/logout" ) )
				.logoutSuccessUrl( "/" ).permitAll()
				.and()
				.rememberMe().key( "test" );
	}
}
