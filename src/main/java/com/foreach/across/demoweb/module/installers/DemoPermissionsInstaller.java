/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.installers;

import com.foreach.across.core.annotations.Installer;
import com.foreach.across.core.annotations.InstallerMethod;
import com.foreach.across.core.installers.InstallerPhase;
import com.foreach.across.modules.user.business.PermissionGroup;
import com.foreach.across.modules.user.business.Role;
import com.foreach.across.modules.user.services.PermissionService;
import com.foreach.across.modules.user.services.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

@Installer(description = "Define demo permissions and assign them to the admin role.",
           phase = InstallerPhase.AfterModuleBootstrap)
public class DemoPermissionsInstaller
{
	private static final Logger LOG = LoggerFactory.getLogger( DemoPermissionsInstaller.class );

	@Autowired
	private RoleService roleService;

	@Autowired
	private PermissionService permissionService;

	@InstallerMethod
	public void install() {
		createPermissionGroupAndPermissions();
		createRegisteredUserRole();
		assignPermissionsToExistingRole();
	}

	private void createPermissionGroupAndPermissions() {
		// Register the permissions - a default group with these permissions will be created if not found
		permissionService.definePermission( "access private section", "The user can access the private section.",
		                                    "demo-permissions" );

		// Update the newly created group with some more descriptive text
		PermissionGroup permissionGroup = permissionService.getPermissionGroup( "demo-permissions" );
		permissionGroup.setTitle( "Module: DemoWebModule" );
		permissionGroup.setDescription(
				"Custom permissions defined by the DemoWebModule to illustrate integration with the UserModule." );

		permissionService.save( permissionGroup );
	}

	private void createRegisteredUserRole() {
		roleService.defineRole( "ROLE_REGISTERED_USER",
		                        "Registered user",
		                        Arrays.asList( "access private section" ) );
	}

	private void assignPermissionsToExistingRole() {
		// Extend the admin role with the new permissions
		Role role = roleService.getRole( "ROLE_ADMIN" );

		if ( role != null ) {
			role.addPermission( "access private section" );
			roleService.save( role );
		}
		else {
			LOG.warn(
					"ROLE_ADMIN does not appear to exist - the demo permissions have not been assigned to any role." );
		}
	}
}
