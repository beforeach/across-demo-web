/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers;

import com.foreach.across.modules.user.business.User;
import com.foreach.across.modules.user.security.CurrentUserProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Holds public controller methods - accessible for anonymous users.
 */
@PublicLayoutController
public class PublicController
{
	@Autowired
	private CurrentUserProxy currentUser;

	@RequestMapping("")
	public String homepage( @AuthenticationPrincipal User user, Model model ) {
		model.addAttribute( "currentUser", currentUser );
		model.addAttribute( "user", user );

		return "th/demoweb/home";
	}

	@RequestMapping("error")
	public ResponseEntity<String> error(){
		return new ResponseEntity<>( "<p>There has been an error processing your request</p>", HttpStatus.OK );
	}
}
