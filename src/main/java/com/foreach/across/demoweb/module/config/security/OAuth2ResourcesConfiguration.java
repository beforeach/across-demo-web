/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.config.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 * Configures the OAuth2 protected resources of the demo website.
 * All /api REST web services are protected using OAuth2 and require a valid access token.
 * <p/>
 * This is in fact also a Spring security configuration.  The ResourceServer is configured in such a
 * way to only match on /api/** requests, meaning that other security filters will handle all non-matching
 * requests. The ResourceServerConfiguration is done by the OAuth2Module, ensuring that this matcher is
 * executed before the wildcard matcher in PrivateSectionConfiguration (because DebugWebModule depends
 * on OAuth2Module).
 *
 * @author Arne Vandamme
 * @since 1.0.3
 */
@Configuration
public class OAuth2ResourcesConfiguration extends ResourceServerConfigurerAdapter
{
	@Override
	public void configure( ResourceServerSecurityConfigurer resources ) throws Exception {
		resources.resourceId( "demo-web" );
	}

	@Override
	public void configure( HttpSecurity http ) throws Exception {
		http.antMatcher( "/api/**" ).authorizeRequests().anyRequest().authenticated();
	}
}
