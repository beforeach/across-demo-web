/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.config;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.foreach.across.core.development.AcrossDevelopmentMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class JsonConverterConfig extends WebMvcConfigurerAdapter
{
	@Autowired
	private AcrossDevelopmentMode developmentMode;

	private SimpleModule jacksonModule = new SimpleModule();

	@Override
	public void configureMessageConverters( List<HttpMessageConverter<?>> converters ) {
		MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
		jsonConverter.getObjectMapper().registerModule( jacksonModule );
		jsonConverter.setPrettyPrint( developmentMode.isActive() );

		converters.add( jsonConverter );
	}
}
