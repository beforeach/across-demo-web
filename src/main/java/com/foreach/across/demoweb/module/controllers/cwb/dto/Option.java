/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.cwb.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Option
{
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String option1;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String option2;

	public String getOption1() {
		return option1;
	}

	public void setOption1( String option1 ) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2( String option2 ) {
		this.option2 = option2;
	}
}
