package com.foreach.across.demoweb.module.controllers;

import com.foreach.across.demoweb.module.templates.PublicLayoutTemplate;
import com.foreach.across.modules.web.template.Template;
import org.springframework.stereotype.Controller;

import java.lang.annotation.*;

/**
 * illustrates
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Controller
@Template(PublicLayoutTemplate.NAME)
public @interface PublicLayoutController
{
}
