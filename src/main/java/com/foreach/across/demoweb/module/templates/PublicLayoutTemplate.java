/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.templates;

import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import com.foreach.across.modules.web.template.LayoutTemplateProcessorAdapterBean;
import org.springframework.stereotype.Component;

@Component
public class PublicLayoutTemplate extends LayoutTemplateProcessorAdapterBean
{
	public static final String NAME = "public";

	public PublicLayoutTemplate() {
		super( NAME, "th/demoweb/layouts/public" );
	}

	@Override
	protected void registerWebResources( WebResourceRegistry registry ) {
		registry.addWithKey( WebResource.JAVASCRIPT, "jquery",
		                     "//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js",
		                     WebResource.EXTERNAL );
		registry.addWithKey( WebResource.CSS, "bootstrap",
		                     "//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css",
		                     WebResource.EXTERNAL );
		registry.addWithKey( WebResource.JAVASCRIPT_PAGE_END, "bootstrap-js",
		                     "//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js",
		                     WebResource.EXTERNAL );
	}
}
