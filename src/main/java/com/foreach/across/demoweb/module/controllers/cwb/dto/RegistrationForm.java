/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.cwb.dto;

import org.apache.commons.lang3.StringUtils;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Andy Somers
 */
public class RegistrationForm
{
	private String firstname;
	private String lastname;
	private String date;
	private String month;
	private String year;
	private String street;
	private String postalcode;
	private Boolean mailyesno;
	private String sex;

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname( String firstname ) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname( String lastname ) {
		this.lastname = lastname;
	}

	public String getDate() {
		return date;
	}

	public void setDate( String date ) {
		this.date = date;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth( String month ) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear( String year ) {
		this.year = year;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet( String street ) {
		this.street = street;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode( String postalcode ) {
		this.postalcode = postalcode;
	}

	public Boolean getMailyesno() {
		return mailyesno;
	}

	public void setMailyesno( Boolean mailyesno ) {
		this.mailyesno = mailyesno;
	}

	public String getSex() {
		return sex;
	}

	public void setSex( String sex ) {
		this.sex = sex;
	}

	private List<String> errors = new LinkedList<>();

	public boolean isComplete() {
		return StringUtils.isNotBlank( firstname ) &&
				StringUtils.isNotBlank( lastname ) &&
				StringUtils.isNotBlank( date ) &&
				StringUtils.isNotBlank( month ) &&
				StringUtils.isNotBlank( year ) &&
				StringUtils.isNotBlank( street ) &&
				StringUtils.isNotBlank( postalcode ) &&
				StringUtils.isNotBlank( sex );
	}

	public boolean isValid() {
		boolean valid = true;
		if ( StringUtils.isNumeric( firstname ) ) {
			valid = false;
			errors.add( "firstname" );
		}
		if ( !StringUtils.isAlphaSpace( lastname ) ) {
			valid = false;
			errors.add( "lastname" );
		}
		if ( StringUtils.isBlank( date ) ) {
			valid = false;
			errors.add( "date" );
		}
		if ( StringUtils.isBlank( month ) ) {
			valid = false;
			errors.add( "month" );
		}
		if ( StringUtils.isBlank( year ) ) {
			valid = false;
			errors.add( "year" );
		}
		if ( !StringUtils.isAlphanumericSpace( street ) ) {
			valid = false;
			errors.add( "street" );
		}
		if ( !StringUtils.isNumeric( postalcode ) ) {
			valid = false;
			errors.add( "postalcode" );
		}
		if ( StringUtils.isBlank( sex ) ) {
			valid = false;
			errors.add( "sex" );
		}
		return valid;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors( List<String> errors ) {
		this.errors = errors;
	}
}
