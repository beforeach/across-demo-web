/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers.cwb;

import com.foreach.across.demoweb.module.controllers.cwb.dto.RegistrationForm;
import com.foreach.across.modules.web.resource.WebResource;
import com.foreach.across.modules.web.resource.WebResourceRegistry;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@CwbLayoutController
@RequestMapping("/cwb")
public class PageController
{
	@RequestMapping({ "", "/" })
	public String homepage() {
		return "th/cwb/index";
	}

	@RequestMapping("tinymce")
	public String tinymce( WebResourceRegistry webResourceRegistry ) {
		webResourceRegistry.addWithKey( WebResource.JAVASCRIPT, "cwb.tinymce", "/js/cwb/tinymce/tiny_mce.js",
		                                WebResource.VIEWS );
		return "th/cwb/tinymce";
	}

	@RequestMapping(value = "upload", method = RequestMethod.GET)
	public String showFileUploadForm() {
		return "th/cwb/upload";
	}

	@RequestMapping(value = "upload", method = RequestMethod.POST)
	public String handleFileUpload( @RequestParam("file") MultipartFile file, Model model ) {
		model.addAttribute( "fileName", file.getOriginalFilename() );
		model.addAttribute( "fileSize", file.getSize() );
		return "th/cwb/upload";
	}

	@RequestMapping(value = "pop-up")
	public String popup() {
		return "th/cwb/popup";
	}

	@RequestMapping(value = "pop-up-contents")
	public String popupContents() {
		return "th/cwb/popup-contents";
	}

	@RequestMapping(value = "tableoverview")
	public String tableoverview() {
		return "th/cwb/tableoverview";
	}

	@RequestMapping(value = "variables")
	public String variables() {
		return "th/cwb/variables";
	}

	@RequestMapping(value = "registrationform", method = RequestMethod.GET)
	public String registrationFormGet( @ModelAttribute(value = "form") RegistrationForm form ) {
		return "th/cwb/registration";
	}

	@RequestMapping(value = "registrationform", method = RequestMethod.POST)
	public String registrationFormPost( @ModelAttribute(value = "form") RegistrationForm form, Model model ) {
		if ( !form.isComplete() ) {
			model.addAttribute( "formCompleted", false );
		}
		else if ( !form.isValid() ) {
			model.addAttribute( "formCompleted", true );
			model.addAttribute( "formErrors", form.getErrors() );
		}
		else {
			model.addAttribute( "formCompleted", true );
		}
		return "th/cwb/registration";
	}

	//	TESTPAGES:
	@RequestMapping("requirejs")
	public String requirejs() {
		return "th/cwb/requirejs";
	}

	@RequestMapping("html5")
	public String html5() {
		return "th/cwb/html5";
	}
}
