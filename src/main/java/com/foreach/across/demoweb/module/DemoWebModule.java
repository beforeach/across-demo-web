/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module;

import com.foreach.across.core.AcrossModule;
import com.foreach.across.core.annotations.AcrossDepends;
import com.foreach.across.core.context.configurer.ApplicationContextConfigurer;
import com.foreach.across.core.context.configurer.ComponentScanConfigurer;
import com.foreach.across.demoweb.module.installers.DemoOAuth2ClientInstaller;
import com.foreach.across.demoweb.module.installers.DemoPermissionsInstaller;
import com.foreach.across.demoweb.module.installers.DemoUserInstaller;
import com.foreach.across.modules.adminweb.AdminWebModule;
import com.foreach.across.modules.oauth2.OAuth2Module;
import com.foreach.across.modules.user.UserModule;

import java.util.Set;

/**
 * @author Arne Vandamme
 */
@AcrossDepends(
		required = { AdminWebModule.NAME, UserModule.NAME, OAuth2Module.NAME },
		optional = "DebugWebModule"
)
public class DemoWebModule extends AcrossModule
{
	/**
	 * <p>The unique name of the module.  Even though the module name is available as a property on the AcrossModule
	 * class, it is considered a best practice to provide it as a public constant as well.</p>
	 * <p>Accessing the module name as a simple String is the way to go for optional dependencies,
	 * where the AcrossModule class might not even be on the classpath.</p>
	 */
	public static final String NAME = "DemoWebModule";
	public static final String RESOURCES = "demoweb";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getResourcesKey() {
		return RESOURCES;
	}

	@Override
	public String getDescription() {
		return "Module representing the DemoWeb functionality.";
	}

	@Override
	public Object[] getInstallers() {
		return new Object[] {
				DemoPermissionsInstaller.class,
				DemoUserInstaller.class,
				DemoOAuth2ClientInstaller.class
		};
	}

	@Override
	protected void registerDefaultApplicationContextConfigurers( Set<ApplicationContextConfigurer> contextConfigurers ) {
		contextConfigurers.add(
				new ComponentScanConfigurer(
						"com.foreach.across.demoweb.module.config",
						"com.foreach.across.demoweb.module.templates",
						"com.foreach.across.demoweb.module.controllers"
				)
		);
	}
}
