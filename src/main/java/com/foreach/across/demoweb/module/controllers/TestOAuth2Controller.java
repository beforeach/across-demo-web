/*
 * Copyright 2014 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.foreach.across.demoweb.module.controllers;

import com.foreach.across.demoweb.module.templates.PublicLayoutTemplate;
import com.foreach.across.modules.web.template.Template;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Arne Vandamme
 */
@Controller
@RequestMapping("/test/oauth2")
public class TestOAuth2Controller
{

	@RequestMapping
	@Template(PublicLayoutTemplate.NAME)
	public String testInfo() {
		return "th/demoweb/test-oauth2";
	}
}
